package swaggerui

import (
	"encoding/json"
	"fmt"
	"io"
	"reflect"
	"strconv"
	"strings"
)

func newEncoder(w io.Writer) *encoder {
	return &encoder{w: w}
}

type encoder struct {
	w io.Writer

	depth        int
	indentPrefix string
	indentValue  string
}

func (enc *encoder) Encode(v any) {
	enc.encode("", v)
}

func (enc *encoder) SetIndent(prefix, indent string) {
	enc.indentPrefix = prefix
	enc.indentValue = indent
}

func (enc *encoder) sub(depth int) *encoder {
	return &encoder{
		w:            enc.w,
		depth:        enc.depth + depth,
		indentPrefix: enc.indentPrefix,
		indentValue:  enc.indentValue,
	}
}

func (enc *encoder) write(value string) {
	_, _ = fmt.Fprintf(enc.w, "%s", value)
}

func (enc *encoder) prefix() {
	enc.write(enc.indentPrefix + strings.Repeat(enc.indentValue, enc.depth))
}

func (enc *encoder) encode(key string, value any) {
	switch to, vo := reflect.TypeOf(value), reflect.ValueOf(value); vo.Kind() {
	case reflect.Pointer:
		enc.encode(key, vo.Elem().Interface())
	case reflect.Struct:
		enc.prefix()
		if key != "" {
			enc.write(key + ": ")
		}
		enc.write("{\n")
		var subEnc = enc.sub(1)
		for i, t := 0, to.NumField(); i < t; i++ {
			if !vo.Field(i).IsZero() {
				subEnc.encode(to.Field(i).Tag.Get("cfg"), vo.Field(i).Interface())
				subEnc.write(",\n")
			}
		}
		enc.prefix()
		enc.write("}")
	case reflect.Slice:
		if vo.Len() > 0 {
			enc.prefix()
			if key != "" {
				enc.write(key + ": ")
			}
			enc.write("[\n")
			var subEnc = enc.sub(1)
			for i, t := 0, vo.Len(); i < t; i++ {
				subEnc.encode("", vo.Index(i).Interface())
				subEnc.write(",\n")
			}
			enc.prefix()
			enc.write("]")
		}
	case reflect.Map:
		if !vo.IsNil() {
			if vo.Type() == reflect.TypeOf(Object{}) {
				if data, err := json.Marshal(vo.Interface()); err == nil {
					enc.prefix()
					if key != "" {
						enc.write(key + ": ")
					}
					enc.write(string(data))
				}
			}
		}
	case reflect.String:
		enc.prefix()
		if key != "" {
			enc.write(key + ": ")
		}
		if vo.Type() == reflect.TypeOf(Item("")) {
			enc.write(vo.String())
		} else {
			enc.write("'" + vo.String() + "'")
		}
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		enc.prefix()
		if key != "" {
			enc.write(key + ": ")
		}
		enc.write(strconv.FormatInt(vo.Int(), 10))
	case reflect.Bool:
		enc.prefix()
		enc.write(key + ": " + strconv.FormatBool(vo.Bool()))
	}
}
