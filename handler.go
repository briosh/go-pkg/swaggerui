package swaggerui

import (
	"embed"
	"io/fs"
	"net/http"
)

//go:embed dist/*
var dist embed.FS

func Handler(url string) http.Handler {
	return HandlerWithConfig(Config{
		URL:         url,
		DomID:       "#swagger-ui",
		DeepLinking: BoolPtr(true),
		Presets: []Item{
			"SwaggerUIBundle.presets.apis",
			"SwaggerUIStandalonePreset",
		},
		Plugins: []Item{
			"SwaggerUIBundle.plugins.DownloadUrl",
		},
		Layout: "StandaloneLayout",
	})
}

func HandlerWithConfig(config Config) http.Handler {
	return handler{initializer: config.Encode()}
}

type handler struct {
	initializer []byte
}

func (h handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path == "swagger-initializer.js" {
		w.Header().Set("Content-Type", "text/javascript; charset=utf-8")
		if _, err := w.Write(h.initializer); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
		return
	}

	rootFS, err := fs.Sub(dist, "dist")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	http.FileServer(http.FS(rootFS)).ServeHTTP(w, r)
}
