/*
Package swaggerui implements Swagger UI handler.

Examples

Simple usage:

	package main

	import (
		"net/http"
		"pkg.brio.sh/swaggerui/v4"
	)

	func main() {
		err := http.ListenAndServe(":8080", swaggerui.Handler("https://petstore.swagger.io/v2/swagger.json"))
		if err != nil {
			panic(err)
		}
	}

*/
package swaggerui
